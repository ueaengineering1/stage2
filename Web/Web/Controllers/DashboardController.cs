﻿namespace Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Web.Models;
    using System.Data.Entity;

    public class DashboardController : Controller
    {
        private ApplicationUserManager _userManager;

        public DashboardController()
        {
        }

        public DashboardController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        // GET: Dashboard
        [HttpGet]
        public ActionResult Index(string semesterId)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                try
                {
                    var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();

                    int semid = Int32.Parse(semesterId);
                    ViewBag.SemId = semid;
                    StudentSemester semester = db.StudentSemesters.Include(x => x.Modules.Select(y => y.Tasks)).Include(x => x.ApplicationUser).FirstOrDefault(entity => entity.Id == semid);
                    if (semester.ApplicationUser.Id != user.Id) return new HttpUnauthorizedResult();
                    ICollection<StudentModule> modules = semester.Modules;
                    LinkedList<StudentTask> tasks = new LinkedList<StudentTask>();

                    ViewBag.Title = semester.SemesterName + " Dashboard";
                    ViewBag.mods = semester.Modules.ToList();
                    foreach (StudentModule m in modules)
                    {
                        foreach (StudentTask t in m.Tasks)
                        {
                            tasks.AddLast(t);
                        }
                    }

                    var ganttList = new List<GanttTask>();
                    var parentList = new List<string>();
                    foreach (StudentTask t in tasks)
                    {
                        if (t.ParentTask != string.Empty)
                        {
                            parentList.Add(t.ParentTask);
                        }

                        var type = "error";
                        switch (t.TaskType)
                        {
                            case StudentTaskType.ACTIVITY:
                                type = "bar-activity";
                                break;
                            case StudentTaskType.COURSEWORK:
                                type = "bar-coursework";
                                break;
                            case StudentTaskType.LAB:
                                type = "bar-lab";
                                break;
                            case StudentTaskType.LECTURE:
                                type = "bar-lecture";
                                break;
                            case StudentTaskType.MILESTONE:
                                type = "bar-milestone";
                                break;
                            case StudentTaskType.SEMINAR:
                                type = "bar-seminar";
                                break;
                            default:
                                break;
                        }

                        ganttList.Add(new GanttTask
                        {
                            start = t.StartDate.ToString("yyyy-MM-dd"),
                            end = t.EndDate.ToString("yyyy-MM-dd"),
                            name = t.Name,
                            id = t.Uuid,
                            progress = " ",
                            dependencies = t.ParentTask,
                            custom_class = type,
                            type = t.TaskType.ToString(),
                            module = t.Module.Name,
                            completed = t.Completion.ToString()
                        });
                    }

                    foreach (GanttTask t in ganttList)
                    {
                        foreach (string p in parentList)
                        {
                            if (t.id == p)
                            {
                                t.progress = "100";
                                if (!t.custom_class.Contains("-parent"))
                                {
                                    t.custom_class += "-parent";
                                }
                            }
                        }
                    }

                    return View(ganttList);
                }
                catch
                {
                    return new HttpNotFoundResult();
                }
            }
            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult UpdateTaskDates(GanttTask tsk)
        {
            try
            {
                var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
                var task = db.StudentTasks.First(a => a.Uuid == tsk.id);
                DateTime.TryParse(tsk.start, out DateTime sd);
                task.StartDate = sd;
                DateTime.TryParse(tsk.end, out DateTime ed);
                task.EndDate = ed;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new
                {
                    ResultValue = "An Exception Occured: " + e.Message
                });
            }
            return Json(new
            {
                ResultValue = "Task Date Updated"
            });
        }

        [HttpGet]
        public JsonResult getNotesList(string moduleId)
        {
            try
            {
                Int32.TryParse(moduleId, out int modId);

                var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
                StudentModule module = db.StudentModules.Include(x => x.ModuleNotes).FirstOrDefault(entity => entity.Id == modId);
                List<StudentModuleNoteViewModel> notes = new List<StudentModuleNoteViewModel>();

                foreach (StudentModuleNote n in module.ModuleNotes)
                {
                    StudentModuleNoteViewModel mnote = new StudentModuleNoteViewModel();
                    mnote.Id = n.Id;
                    mnote.Name = n.Name;
                    notes.Add(mnote);
                }

                return Json(
                    notes,
                    JsonRequestBehavior.AllowGet
                );
            }
            catch(Exception e)
            {
                return Json(new
                {
                    ResultValue = "An error occured: " + e.Message
                });
            }
        }
    }
}