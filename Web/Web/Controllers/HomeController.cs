﻿namespace Web.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Xml.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Web.Models;
    using System.Data.Entity;

    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    var user = UserManager.FindById(User.Identity.GetUserId());
                    ViewBag.UserNow = user;

                    var semesters = db.StudentSemesters.Include("ApplicationUser").Where(s => s.ApplicationUser.Id == user.Id).ToList<StudentSemester>();

                    if (semesters != null)
                    {
                        return View(semesters.ToList());
                    }
                    return View();
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult CreateSemesterModal()
        {
            if (TempData["Message"] != null) ViewBag.Message = TempData["Message"];
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendContactForm(Models.ContactModel mod)
        {
            //Web.Models.ContactModel c = new Web.Models.ContactModel(mod.name, mod.email, mod.message);
            //Need to provide feedback.
            TempData["cfResult"] = mod.sendEmail();
            return RedirectToAction("Contact");
        }


        [HttpPost]
        public ActionResult UploadSemesterFile(CreateSemesterViewModel input)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            if (input.File != null && input.File.ContentLength > 0)
            {
                try
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(input.File.InputStream);

                    XDocument doc = XDocument.Load(sr);
                    var reader = doc.CreateReader();
                    var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();

                    StudentSemester semester = new StudentSemester
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        SemesterName = input.Name,
                        ApplicationUser = user
                    };
                    db.StudentSemesters.Add(semester);

                    if (reader.ReadToDescendant("UserModules"))
                    {
                        if (reader.ReadToDescendant("module"))
                        {
                            do
                            {
                                StudentModule module = new StudentModule
                                {
                                    Uuid = reader.GetAttribute("uuid"),
                                    Name = reader.GetAttribute("name"),
                                    StartDate = DateTime.Parse(reader.GetAttribute("startDate")),
                                    EndDate = DateTime.Parse(reader.GetAttribute("endDate")),
                                    ModuleCode = reader.GetAttribute("moduleCode"),
                                    ModuleOrganiser = reader.GetAttribute("moduleOrganiser"),
                                    CourseworkAmount = float.Parse(reader.GetAttribute("courseworkAmount")),
                                    Exam = bool.Parse(reader.GetAttribute("exam")),
                                    Semester = semester
                                };

                                var moduleTest = db.StudentModules.Include(x => x.Semester.ApplicationUser).FirstOrDefault(x => x.Uuid == module.Uuid 
                                    && x.Semester.ApplicationUser.Id == user.Id);
                                if (moduleTest != null)
                                {
                                    throw new Exception("This file has already been imported!");
                                }

                                db.StudentModules.Add(module);

                                if (reader.ReadToDescendant("ModuleNotes"))
                                {
                                    if (reader.ReadToDescendant("note"))
                                    {
                                        do
                                        {
                                            StudentModuleNote note = new StudentModuleNote
                                            {
                                                Uuid = reader.GetAttribute("uuid"),
                                                Name = reader.GetAttribute("name"),
                                                Content = reader.GetAttribute("text"),
                                                CreatedDate = DateTime.Now,
                                                Module = module
                                            };
                                            db.StudentModuleNotes.Add(note);
                                        } while (reader.ReadToNextSibling("note"));
                                    }
                                }

                                if (reader.ReadToFollowing("ModuleTasks"))
                                {
                                    if (reader.ReadToDescendant("task"))
                                    {
                                        do
                                        {
                                            StudentTask task = new StudentTask
                                            {
                                                Uuid = reader.GetAttribute("uuid"),
                                                Name = reader.GetAttribute("name"),
                                                StartDate = DateTime.Parse(reader.GetAttribute("startDate")),
                                                EndDate = DateTime.Parse(reader.GetAttribute("endDate")),
                                                Description = reader.GetAttribute("description"),
                                                Completion = bool.Parse(reader.GetAttribute("completion")),
                                                TaskType = (StudentTaskType)Enum.Parse(typeof(StudentTaskType), reader.GetAttribute("type")),
                                                Module = module,
                                                ParentTask = reader.GetAttribute("parent")
                                            };
                                            db.StudentTasks.Add(task);

                                            if (reader.ReadToDescendant("TaskNotes")) {
                                                if (reader.ReadToDescendant("note")) {
                                                    do
                                                    {
                                                        StudentTaskNote note = new StudentTaskNote
                                                        {
                                                            Uuid = reader.GetAttribute("uuid"),
                                                            Name = reader.GetAttribute("name"),
                                                            Content = reader.GetAttribute("text"),
                                                            CreatedDate = DateTime.Now,
                                                            Task = task
                                                        };
                                                        db.StudentTaskNotes.Add(note);
                                                    } while (reader.ReadToNextSibling("note"));
                                                    reader.ReadEndElement();
                                                }
                                            }
                                        } while (reader.ReadToNextSibling("task"));

                                        reader.ReadEndElement();
                                    }
                                }

                            } while (reader.ReadToNextSibling("module"));

                        }
                    }

                    db.SaveChanges();
                }
                catch (Exception ex) {
                    TempData["Message"] = "ERROR:" + ex.Message.ToString();
                    return RedirectToAction("createSemesterModal", "Home");
                }
            } else {
                TempData["Message"] = "You have not specified a file.";
            }

            TempData["Message"] = "Success! upload another semester or click outside this window to close";

            return RedirectToAction("createSemesterModal", "Home");
        }
    }
}