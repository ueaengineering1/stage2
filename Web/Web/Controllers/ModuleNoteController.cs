﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.Data.Entity;

namespace Web.Controllers
{
    public class ModuleNoteController : Controller
    {
        private ApplicationUserManager _userManager;

        public ModuleNoteController()
        {
        }

        public ModuleNoteController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(string moduleId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        ViewBag.ModuleId = moduleId;
                        if (TempData["Message"] != null) ViewBag.Message = TempData["Message"];
                        return View();
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }
            return new HttpUnauthorizedResult();
        }

        [HttpGet]
        public ActionResult View(string noteId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        int nid = Int32.Parse(noteId);
                        StudentModuleNote n = db.StudentModuleNotes.Include(x => x.Module).FirstOrDefault(x => x.Id == nid);
                        StudentModuleNoteViewModel nvm = new StudentModuleNoteViewModel
                        {
                            Id = n.Id,
                            Name = n.Name,
                            Content = n.Content,
                            ModuleId = n.Module.Id
                        };

                        ViewBag.NoteId = noteId;

                        if(TempData["Message"] != null) ViewBag.Message = TempData["Message"];
                        return View(nvm);
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }
            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Add(StudentModuleNoteViewModel note)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentModule mod = db.StudentModules.FirstOrDefault(x => x.Id == note.ModuleId);

                        StudentModuleNote m = new StudentModuleNote
                        {
                            Name = note.Name,
                            Uuid = Guid.NewGuid().ToString(),
                            CreatedDate = DateTime.Now,
                            Module = mod,
                            Content = note.Content
                        };
                        db.StudentModuleNotes.Add(m);
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully added to module, Click outside modal to exit";

                        return RedirectToAction("Index", "ModuleNote");

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Update(StudentModuleNoteViewModel note)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentModuleNote modnote = db.StudentModuleNotes.FirstOrDefault(x => x.Id == note.Id);

                        modnote.Name = note.Name;
                        modnote.Content = note.Content;
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully updated, Click outside modal to exit";

                        return RedirectToAction("View", "ModuleNote", new { noteId = note.Id });

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Delete(StudentModuleNoteViewModel note)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentModuleNote modnote = db.StudentModuleNotes.FirstOrDefault(x => x.Id == note.Id);

                        db.StudentModuleNotes.Remove(modnote);
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully deleted, Click outside modal to exit";

                        return RedirectToAction("Index", "ModuleNote");

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }
    }
}