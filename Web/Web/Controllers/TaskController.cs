﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Web.Models;

namespace Web.Controllers
{
    public class TaskController : Controller
    {
        private ApplicationUserManager _userManager;

        public TaskController()
        {
        }

        public TaskController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        public ActionResult Index(string semesterId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        int semid = Int32.Parse(semesterId);
                        var semester = db.StudentSemesters.Include(x => x.Modules.Select(y => y.Tasks)).FirstOrDefault(entity => entity.Id == semid);
                        ICollection<StudentModule> modules = semester.Modules.ToList();
                        ViewBag.Modules = modules;
                        ViewBag.Tasks = modules.First().Tasks.ToList();

                        if (TempData["Message"] != null) ViewBag.Message = TempData["Message"];

                        return View();
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpGet]
        public ActionResult View(string taskUuid)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        var task = db.StudentTasks
                            .Include(x => x.TaskNotes)
                            .Include(x => x.Module)
                            .Include(x => x.Module.Semester.ApplicationUser)
                            .FirstOrDefault(x => x.Uuid == taskUuid && x.Module.Semester.ApplicationUser.Id == user.Id);

                        TaskViewModel vTask = new TaskViewModel
                        {
                            Id = task.Id,
                            Uuid = task.Uuid,
                            Name = task.Name,
                            StartDate = task.StartDate,
                            EndDate = task.EndDate,
                            Description = task.Description,
                            TaskType = task.TaskType,
                            Completion = task.Completion,
                            Module = task.Module.Id,
                            ParentTask = task.ParentTask
                        };

                        vTask.TaskNotes = new List<TaskNoteViewModel>();

                        foreach (StudentTaskNote note in task.TaskNotes)
                        {
                            TaskNoteViewModel tn = new TaskNoteViewModel
                            {
                                Id = note.Id,
                                Name = note.Name,
                                Content = note.Content,
                                CreatedDate = note.CreatedDate
                            };
                            vTask.TaskNotes.Add(tn);
                        }

                        var semester = db.StudentSemesters.Include(x => x.Modules.Select(y => y.Tasks)).FirstOrDefault(entity => entity.Id == task.Module.Semester.Id);
                        ICollection<StudentModule> modules = semester.Modules.ToList();
                        ViewBag.Modules = modules;
                        ViewBag.Tasks = modules.First().Tasks.ToList();
                        if (TempData["Message"] != null) ViewBag.Message = TempData["Message"];

                        return View(vTask);
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Update(TaskViewModel task)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        var taskDb = db.StudentTasks.FirstOrDefault(x => x.Id == task.Id);

                        taskDb.Name = task.Name;
                        taskDb.ParentTask = task.ParentTask;
                        taskDb.StartDate = task.StartDate;
                        taskDb.TaskType = task.TaskType;
                        taskDb.Module = db.StudentModules.FirstOrDefault(x => x.Id == task.Module);
                        taskDb.Completion = task.Completion;
                        taskDb.Description = task.Description;
                        taskDb.EndDate = task.EndDate;

                        db.SaveChanges();

                        TempData["Message"] = "Task has been updated, Click outside the modal to continue";

                        return RedirectToAction("View", "Task", new { taskUuid = task.Uuid });
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Add(AddTaskViewModel task)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentModule module = db.StudentModules.Include(x => x.Semester).FirstOrDefault(entity => entity.Id == task.Module);
                        StudentTask dbTask = new StudentTask
                        {
                            Name = task.Name,
                            Uuid = Guid.NewGuid().ToString(),
                            StartDate = task.StartDate,
                            EndDate = task.EndDate,
                            Description = task.Description,
                            TaskType = task.TaskType,
                            Completion = false,
                            ParentTask = task.ParentTask,
                            Module = module
                        };

                        db.StudentTasks.Add(dbTask);
                        db.SaveChanges();

                        TempData["Message"] = "Task added, click outside modal to close or add another.";

                        return RedirectToAction("Index", "Task", new { semesterId = module.Semester.Id });
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpGet]
        public ActionResult ModTasks(string moduleId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        int modid = Int32.Parse(moduleId);
                        var module = db.StudentModules.Include(x => x.Tasks).FirstOrDefault(entity => entity.Id == modid);
                        List<StudentTask> tasks = module.Tasks.ToList();

                        List<GetModuleTasksAjaxViewModel> viewTasks = new List<GetModuleTasksAjaxViewModel>();

                        foreach (StudentTask t in tasks)
                        {
                            GetModuleTasksAjaxViewModel tvm = new GetModuleTasksAjaxViewModel();
                            tvm.Name = t.Name;
                            tvm.Uuid = t.Uuid;
                            viewTasks.Add(tvm);
                        }

                        return Json(viewTasks, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Delete(TaskViewModel task)
        {
            if (User.Identity.IsAuthenticated)
            {
                var Uuid = "";
                var user = UserManager.FindById(User.Identity.GetUserId());
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        var dbtask = db.StudentTasks.Include(x => x.TaskNotes).Include(x => x.Module.Tasks).FirstOrDefault(x => x.Id == task.Id);
                        Uuid = dbtask.Uuid;
                        foreach(StudentTaskNote tn in dbtask.TaskNotes.ToList())
                        {
                            db.StudentTaskNotes.Remove(tn);
                        }

                        foreach(StudentTask t in dbtask.Module.Tasks)
                        {
                            if(t.ParentTask == dbtask.Uuid)
                            {
                                t.ParentTask = string.Empty;
                            }
                        }

                        db.StudentTasks.Remove(dbtask);
                        db.SaveChanges();

                        var semester = db.StudentSemesters.Include(x => x.ApplicationUser).FirstOrDefault(x => x.ApplicationUser.Id == user.Id);

                        TempData["Message"] = "Task succesfully deleted, Click outside modal to exit";

                        return RedirectToAction("Index", "Task", new { semesterId = semester.Id });
                    }
                    catch (Exception e)
                    {
                        TempData["Message"] = "Error: Unable to delete task!";
                        return RedirectToAction("View", "Task", new { taskUuid = Uuid });
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }
    }
}