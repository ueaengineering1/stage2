﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.Data.Entity;

namespace Web.Controllers
{
    public class TaskNoteController : Controller
    {
        private ApplicationUserManager _userManager;

        public TaskNoteController()
        {
        }

        public TaskNoteController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(string taskId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        ViewBag.TaskId = taskId;
                        if (TempData["Message"] != null) ViewBag.Message = TempData["Message"];
                        return View();
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }
            return new HttpUnauthorizedResult();
        }

        [HttpGet]
        public ActionResult View(string noteId)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        int nid = Int32.Parse(noteId);
                        StudentTaskNote n = db.StudentTaskNotes.FirstOrDefault(x => x.Id == nid);
                        TaskNoteViewModel nvm = new TaskNoteViewModel
                        {
                            Id = n.Id,
                            Name = n.Name,
                            Content = n.Content,
                        };

                        ViewBag.NoteId = noteId;

                        if(TempData["Message"] != null) ViewBag.Message = TempData["Message"];
                        return View(nvm);
                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }
            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Add(TaskNoteViewModel note)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentTask task = db.StudentTasks.FirstOrDefault(x => x.Id == note.TaskId);

                        StudentTaskNote t = new StudentTaskNote
                        {
                            Name = note.Name,
                            Uuid = Guid.NewGuid().ToString(),
                            CreatedDate = DateTime.Now,
                            Task = task,
                            Content = note.Content
                        };
                        db.StudentTaskNotes.Add(t);
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully added to task, Click outside modal to exit or add another note.";

                        return RedirectToAction("View", "Task", new { taskUuid = task.Uuid });

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Update(TaskNoteViewModel note)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentTaskNote tasknote = db.StudentTaskNotes.Include(x => x.Task).FirstOrDefault(x => x.Id == note.Id);

                        tasknote.Name = note.Name;
                        tasknote.Content = note.Content;
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully updated, Click outside modal to exit";

                        return RedirectToAction("View", "Task", new { taskUuid = tasknote.Task.Uuid });

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        public ActionResult Delete(TaskNoteViewModel task)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>())
                {
                    try
                    {
                        StudentTaskNote tasknote = db.StudentTaskNotes.Include(x => x.Task).FirstOrDefault(x => x.Id == task.Id);
                        var tUuid = tasknote.Task.Uuid;

                        db.StudentTaskNotes.Remove(tasknote);
                        db.SaveChanges();

                        TempData["Message"] = "Note succesfully deleted, Click outside modal to exit";

                        return RedirectToAction("View", "Task", new { taskUuid = tUuid });

                    }
                    catch
                    {
                        return new HttpNotFoundResult();
                    }
                }
            }

            return new HttpUnauthorizedResult();
        }
    }
}