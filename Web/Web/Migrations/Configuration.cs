namespace Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Web.Models.ApplicationDbContext context)
        {
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO AspNetUsers (Id,Uuid,StudentName,School,Course,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber, PhoneNumberConfirmed,TwoFactorEnabled, LockoutEndDateUtc, LockoutEnabled, AccessFailedCount, UserName) 
                VALUES(1, 'bb4c9fec-a07d-4c0e-93ee-3e9068f01250', 'Alex Norris', 'CMP', 'Computing Science', 'fakeEmail@myEmail.com', 1, 'AGOrGNYbgxtSmJbfjt9Fs94Q9LaYTjawAHgyArXM3Rn9J2saB+DO7yYxRaEYgqwtXQ==', '4888a19c-78f5-4ba5-b08c-efa8bced3208', '07710 010445', 1, 0, 10 / 03 / 2021, 0, 0, 'AlexNorris');
                            SET IDENTITY_INSERT StudentSemesters ON
                INSERT INTO StudentSemesters(Id, Uuid, StartDate, EndDate, SemesterName, ApplicationUser_Id)
                VALUES(1, '1932af8f-2c4f-418f-ae52-ad839cb8d517', CAST('2019-09-22 09:00:00' AS datetime), CAST('2019-12-20 22:00:00' AS datetime), 'Summer', 1);
                            SET IDENTITY_INSERT StudentSemesters OFF
                            SET IDENTITY_INSERT StudentModules ON
                INSERT INTO StudentModules(Id, Uuid, Name, StartDate, EndDate, Exam, CourseworkAmount, ModuleCode, ModuleOrganiser, Semester_Id)
                VALUES(1, 'b1feee86-387c-4334-a237-7b00cc8eb222', 'GRAPHICS 1', CAST('2019-09-23 09:00:00' AS datetime), CAST('2019-12-10 23:00:00' AS datetime), 1, 40, 'CMP-6028B', 'Stephan Laycock', 1);
                            INSERT INTO StudentModules(Id, Uuid, Name, StartDate, EndDate, Exam, CourseworkAmount, ModuleCode, ModuleOrganiser, Semester_Id)
                VALUES(2, 'a2df2de4-7ab3-47bc-b3ff-eaef923f3ab2', 'PROGRAMMING 2', CAST('2019-09-23 08:00:00' AS datetime), CAST('2019-12-10 22:00:00' AS datetime), 1, 60, 'CMP-6034B', 'Gavin Cawley', 1);
                            INSERT INTO StudentModules(Id, Uuid, Name, StartDate, EndDate, Exam, CourseworkAmount, ModuleCode, ModuleOrganiser, Semester_Id)
                VALUES(3, 'eb4b4a07-cfe4-4e73-8b9e-b3dd56fc6353', 'SOFTWARE ENGINEERING 1', CAST('2019-09-23 09:00:00' AS datetime), CAST('10-12-2019 23:00:00' AS datetime), 0, 100, 'CMP-5012B', 'Rudy Lapeer', 1);
                            INSERT INTO StudentModules(Id, Uuid, Name, StartDate, EndDate, Exam, CourseworkAmount, ModuleCode, ModuleOrganiser, Semester_Id)
                VALUES(4, '70515415-4892-4548-a9fd-a6efc7855e3b', 'DATA STRUCTURES AND ALGORITHMS', CAST('2019-09-23 09:00:00' AS datetime), CAST('2019-12-10 23:00:00' AS datetime), 1, 50, 'CMP-5014Y', 'Tony Bagnall', 1);
                            SET IDENTITY_INSERT StudentModules OFF
                SET IDENTITY_INSERT StudentModuleNotes ON
                INSERT INTO StudentModuleNotes(Id, Uuid, Name, Content, CreatedDate, Module_Id)
                VALUES(1, '692ff979-1650-4952-8865-c66b3d7406d8', 'ModuleNote', 'This is a test note', CAST('2019-10-03 13:00:00' AS datetime), 1);
                            INSERT INTO StudentModuleNotes(Id, Uuid, Name, Content, CreatedDate, Module_Id)
                VALUES(2, '489c320c-3eff-4bc4-a2a6-2584e02eff56', 'AnotherModuleNote', 'Test Note', CAST('2019-11-05 16:00:00' AS datetime), 2);
                            INSERT INTO StudentModuleNotes(Id, Uuid, Name, Content, CreatedDate, Module_Id)
                VALUES(3, 'a59c692b-3e44-4407-8809-825b2305f7a3', 'TestModule', 'test', CAST('2019-10-20 15:00:00' AS datetime), 3);
                            INSERT INTO StudentModuleNotes(Id, Uuid, Name, Content, CreatedDate, Module_Id)
                VALUES(4, '60b60e56-98ac-40af-893c-6116d24c4991', 'Test', 'this is a note centent', CAST('2019-11-10 20:00:00' AS datetime), 4);
                            SET IDENTITY_INSERT StudentModuleNotes OFF
                SET IDENTITY_INSERT StudentTasks ON
                INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(1, 'ffa4932b-63ab-411f-aef5-7f054aedf36f', 'Lecture', CAST('2019-10-01 13:23:20' AS datetime), CAST('2019-12-05 09:00:00' AS datetime), 'Lecture', 1, 0, null, 1);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(2, '4b5f3aa2-46e5-400a-be6b-618fd4fd8af3', 'CourseWork', CAST('2019-12-06 13:20:30' AS datetime), CAST('2019-12-10 14:30:00' AS datetime), 'CourseWork', 3, 0, 'ffa4932b-63ab-411f-aef5-7f054aedf36f', 1);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(3, 'b5e93f8e-327d-4a28-845d-71b3901e5662', 'Lab', CAST('2019-11-20 14:50:20' AS datetime), CAST('2019-11-30 15:30:00' AS datetime), 'Lab', 0, 0, null, 1);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(4, 'dea73b42-29f7-4e5a-997a-086c632546c3', 'Coursework', CAST('2019-09-23 14:20:20' AS datetime), CAST('2019-09-30 17:30:30' AS datetime), 'CourseWork', 3, 0, null, 2);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(5, '9ab71434-e557-47a6-bb13-416b7e0b4533', 'Lecture', CAST('2019-10-03 15:30:00' AS datetime), CAST('2019-10-15 17:30:00' AS datetime), 'Lecture', 1, 0, 'dea73b42-29f7-4e5a-997a-086c632546c3', 2);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(6, '828c2891-3d45-444e-ac32-dcec9d5583fc', 'Seminar', CAST('2019-10-18 12:00:00' AS datetime), CAST('2019-11-11 14:30:00' AS datetime), 'Seminar', 2, 0, '9ab71434-e557-47a6-bb13-416b7e0b4533', 2);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(7, 'a70e15e2-958b-4c74-8c7d-d050eb6cc234', 'Lab', CAST('2019-09-23 14:30:00' AS datetime), CAST('2019-10-15 17:30:00' AS datetime), 'Lab', 0, 0, null, 3);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(8, '2f70b66a-2d61-42dc-becb-3f07e6409e24', 'Lecture', CAST('2019-10-05 13:20:00' AS datetime), CAST('10-10-2019 14:00:00' AS datetime), 'Lecture', 1, 0, null, 3);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(9, '1cee1ce4-1e88-4203-87f2-4cee2b560931', 'CourseWork', CAST('2019-10-10 17:00:00' AS datetime), CAST('2019-10-20 18:00:00' AS datetime), 'CourseWork', 3, 0, '2f70b66a-2d61-42dc-becb-3f07e6409e24', 3);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(10, 'b6e32d21-6a1f-4771-a467-1f18c61f2d68', 'Lecture', CAST('2019-09-24 15:00:00' AS datetime), CAST('2019-11-20 18:00:00' AS datetime), 'Lecture', 1, 0, null, 4);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(11, 'ad5f528b-0e7f-44ce-a8aa-6210b0a5e8f7', 'CourseWork', CAST('2019-11-25 16:00:00' AS datetime), CAST('2019-12-05 12:00:00' AS datetime), 'CourseWork', 3, 0, 'b6e32d21-6a1f-4771-a467-1f18c61f2d68', 4);
                            INSERT INTO StudentTasks(Id, Uuid, Name, StartDate, EndDate, Description, TaskType, Completion, ParentTask, Module_Id)
                VALUES(12, '0232d06a-ada1-46dc-ab43-818585ace1ea', 'Lab', CAST('2019-11-30 13:00:00' AS datetime), CAST('2019-12-10 09:00:00' AS datetime), 'Lab', 0, 0, null, 4);
                            SET IDENTITY_INSERT StudentTasks OFF
                SET IDENTITY_INSERT StudentTaskNotes ON
                INSERT INTO StudentTaskNotes(Id, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(1, 'b73fc7bd-1987-4fd4-818e-f2f67850a902', 'Test Notes', 'Notes for me', CAST('2019-11-11 12:17:50' AS datetime), 1);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(2, '06261947-04ec-4efe-b3a5-6c26fdec5f63', 'notes', 'content', CAST('2019-10-12 13:12:30' AS datetime), 1);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(3, '8d2a3f7f-6271-4a68-9278-a346e146e950', 'Test', 'more content', CAST('2019-12-07 14:00:00' AS datetime), 2);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(4, '1af98e8f-cab2-474d-b025-065eeac68cda', 'my Test', 'another content', CAST('2019-12-08 13:00:00' AS datetime), 2);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(5, '56be9d74-b360-4ea0-ac2d-3a4afccc06e9', 'your tests', 'best content', CAST('2019-09-25 15:00:00' AS datetime), 4);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(6, 'd084d680-f582-4d51-8677-e4fef42f2e1e', 'a tests', 'alot of content', CAST('2019-09-29 16:00:00' AS datetime), 4);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(7, '71477a6c-a313-480e-bf58-f2637294154f', 'Another test', 'much content', CAST('2019-11-29 13:00:00' AS datetime), 11);
                            INSERT INTO StudentTaskNotes(ID, Uuid, Name, Content, CreatedDate, Task_Id)
                VALUES(8, '5ad83912-ce10-4e90-b28d-4548470316d1', 'best Test', 'an amount of content', CAST('2019-11-01 12:00:00' AS datetime), 12);
                SET IDENTITY_INSERT StudentTaskNotes OFF"
            );
        }
    }
}
