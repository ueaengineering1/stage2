﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Security;
using System.Web;

namespace Web.Models
{
    public class ContactModel
    {
        public string name { get; set; }
        public string email { get; set; }
        public string message { get; set; }

        

        public ContactModel()
        {
            name = "";
            email = "";
            message = "";
        }

        public ContactModel(string name, string email, string message)
        {
            this.name = name;
            this.email = email;
            this.message = message;
        }

        public string sendEmail()
        {
            bool existingError = false;
            string result = "";
            if (String.IsNullOrWhiteSpace(name))
            {
                result = "Invalid Name";
                existingError = true;
            }

            if (!isValidEmail(email))
            {
                if (existingError)
                {
                    result = result + " and Invalid Email";
                }
                else
                {
                    existingError = true;
                    result = "Invalid Email";
                }
            }

            if (String.IsNullOrWhiteSpace(message))
            {
                if (existingError)
                {
                    result = result + " and Empty Message";
                }
                else
                {
                    existingError = true;
                    result = "Please Enter a Message";
                }
            }

            if (existingError)
            {
                return result;
            }

            ResourceManager r = new ResourceManager("Web.Properties.SMTPCredentials", Assembly.GetExecutingAssembly());

            var m = new MimeMessage();
            m.From.Add(new MailboxAddress(name, email));
            m.To.Add(new MailboxAddress("My Plan", "myplangroup12@gmail.com"));
            m.Subject = "Contact Form " + System.DateTime.Now;
            m.Body = new TextPart("plain")
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate(r.GetString("emailSendLogin"), r.GetString("emailSendPassword"));
                client.Send(m);
                client.Disconnect(true);
            }
            return "Message Sent";
        }

        //https://forums.asp.net/t/1957656.aspx?Validate+an+email+address+using+c+
        private bool isValidEmail(string email)
        {
            try
            {
                var a = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}