﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class CreateSemesterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [Display(Name = "Semester Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Semester Start")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "Semester End")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "Import File (.xml)")]
        public HttpPostedFileBase File { get; set; }
    }
}