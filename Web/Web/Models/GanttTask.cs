﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class GanttTask
    {
        public string start { get; set; }

        public string end { get; set; }

        public string name { get; set; }

        public string id { get; set; }

        public string progress { get; set; }

        public string dependencies { get; set; }

        public string custom_class { get; set; }

        public string type { get; set; }

        public string module { get; set; }

        public string completed { get; set; }
    }

}