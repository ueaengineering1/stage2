﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Uuid { get; set; }

        public string StudentName { get; set; }

        public string School { get; set; }

        public string Course { get; set; }

        public ICollection<StudentSemester> Semesters { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<StudentSemester> StudentSemesters { get; set; }

        public DbSet<StudentModule> StudentModules { get; set; }

        public DbSet<StudentModuleNote> StudentModuleNotes { get; set; }

        public DbSet<StudentTask> StudentTasks { get; set; }

        public DbSet<StudentTaskNote> StudentTaskNotes { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}