﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class StudentModule
    {
        public int Id { get; set; }

        public string Uuid { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public bool Exam { get; set; }

        [Required]
        public float CourseworkAmount { get; set; }

        [Required]
        public string ModuleCode { get; set; }

        [Required]
        public string ModuleOrganiser { get; set; }

        public StudentSemester Semester { get; set; }

        public ICollection<StudentModuleNote> ModuleNotes { get; set; }

        public ICollection<StudentTask> Tasks { get; set; }
    }
}