﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class StudentSemester
    {
        public int Id { get; set; }

        public string Uuid { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public string SemesterName { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public ICollection<StudentModule> Modules { get; set; }
    }
}