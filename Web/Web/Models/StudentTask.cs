﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class StudentTask
    {
        public int Id { get; set; }

        public string Uuid { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public string Description { get; set; }

        [Required]
        public StudentTaskType TaskType { get; set; }

        public bool Completion { get; set; }

        public StudentModule Module { get; set; }

        public ICollection<StudentTaskNote> TaskNotes { get; set; }

        public string ParentTask { get; set; }
    }

    public enum StudentTaskType
    {
        LAB,
        LECTURE,
        SEMINAR,
        COURSEWORK,
        MILESTONE,
        ACTIVITY
    }
}