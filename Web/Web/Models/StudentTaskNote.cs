﻿namespace Web.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class StudentTaskNote
    {
        public int Id { get; set; }

        public string Uuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Content { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public StudentTask Task { get; set; }
    }
}