﻿using System;

namespace Web.Models
{
    public class TaskNoteViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public int TaskId { get; set; }
    }
}