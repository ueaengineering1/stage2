﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class TaskViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Uuid { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public string Description { get; set; }

        [Required]
        public StudentTaskType TaskType { get; set; }

        [Required]
        public bool Completion { get; set; }

        [Required]
        public int Module { get; set; }

        public List<TaskNoteViewModel> TaskNotes { get; set; }

        public string ParentTask { get; set; }
    }
}